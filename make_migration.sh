docker-compose -f docker-compose.local-dev.yml exec rest_api flask db init
docker-compose -f docker-compose.local-dev.yml exec rest_api flask db migrate -m 'Initial migration'
docker-compose -f docker-compose.local-dev.yml exec rest_api flask db upgrade

