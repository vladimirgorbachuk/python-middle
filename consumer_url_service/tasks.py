import requests
from requests.exceptions import InvalidURL, MissingSchema
from concurrent.futures import ProcessPoolExecutor
from queries.urls import get_all_urls
from queries.events import insert_all_events
from celery import Celery
#import celery_config

STANDARD_TIMEOUT_SECONDS = 15
STANDARD_INTERVAL_URL_CHECK = 5*60


def check_url(url_id, url):
    try:
        resp = requests.get(url)
        return (url_id, resp,)
    except InvalidURL:
        return (url_id, None)
    except MissingSchema:
        return (url_id, None)


app = Celery('tasks', broker='amqp://guest:guest@rabbitmq:5672')
# app.config_from_object(celery_config)


@app.task(name='try_urls')
def try_urls():
    url_tuples = get_all_urls()
    results_list = [check_url(url['id'], url['path']) for url in url_tuples]
    insert_all_events(results_list)
