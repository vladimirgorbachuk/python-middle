import os
import psycopg2

db_connection = psycopg2.connect(
    host="db",
    database=os.environ['POSTGRES_DB'],
    user=os.environ['POSTGRES_USER'],
    password=os.environ['POSTGRES_PASSWORD'])
