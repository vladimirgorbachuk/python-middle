from . import db_connection
from psycopg2.extras import RealDictCursor, execute_batch, execute_values
from collections import namedtuple


EventTuple = namedtuple(
    'EventTuple',
    ['url_id', 'url', 'created', 'response_time', 'status_code', 'response_size', 'active'])


def insert_all_events(url_id_response_tuples_list):
    args_to_insert_list = []
    for url_id_response_tuple in url_id_response_tuples_list:
        url_id = url_id_response_tuple[0]
        response = url_id_response_tuple[1]
        if response:
            #url = response.url
            created = response.headers['Date']
            response_time = int(response.elapsed.total_seconds()*1000)
            status_code = int(response.status_code)
            response_size = len(response.content)
            tuple_to_insert = (
                url_id, created, response_time, status_code, response_size, 'True')
            args_to_insert_list.append(tuple_to_insert)
            print('that is the tuple we insert', tuple_to_insert)
    print('the args list I pass to insert query', args_to_insert_list)
    with db_connection as conn:
        with conn.cursor() as cur:
            execute_values(cur,
                           '''INSERT INTO event (url_id, created, response_time, status_code, response_size, active) VALUES %s''',
                           args_to_insert_list)
