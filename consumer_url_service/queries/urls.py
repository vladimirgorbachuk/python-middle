from . import db_connection
from psycopg2.extras import RealDictCursor


def get_all_urls():
    with db_connection as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute('''SELECT * from url''')
            urls = cur.fetchall()
    return urls
