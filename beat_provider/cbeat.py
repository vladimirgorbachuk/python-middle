import os
from celery import Celery
#import celery_config


celery = Celery(
    broker='amqp://guest:guest@rabbitmq:5672')
# celery.config_from_object(celery_config)
celery.conf.beat_schedule = {
    'refresh': {
        'task': 'try_urls',
        'schedule': float(os.environ['URL_CHECKING_INTERVAL_SECONDS']),
    },
}
