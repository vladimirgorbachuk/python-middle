from . import db
from exceptions.user import TokenDoesntMatchAnyUserException


class User(db.Model):
    '''
    <users>
    * id
    * token  | Токен доступа (для доступа к методам)
    * created  | Дата создания записи
    login  | Имя пользователя
    '''
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True,
                   autoincrement=True, nullable=False)
    token = db.Column(db.String(128), nullable=True, unique=True)
    created = db.Column(db.DateTime, default=db.func.now())
    login = db.Column(db.String(128), nullable=True, unique=True)
