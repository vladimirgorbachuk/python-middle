from sqlalchemy.orm import backref
from . import db
from .user import User


class Url(db.Model):
    '''
    * id
    * path  | Путь к Web-ресурсу
    * created  | Дата создания записи
    * user_id  | Идентификатор пользователя
    title  | Название Web-ресурса
    '''

    id = db.Column(db.Integer, primary_key=True,
                   autoincrement=True, nullable=False, unique=True)
    path = db.Column(db.String(512), nullable=False)
    created = db.Column(db.DateTime, default=db.func.now())
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    owner = db.relationship(
        User, backref=backref('urls', cascade="all,delete"))

    title = db.Column(db.String(128), nullable=True)
