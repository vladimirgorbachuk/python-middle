from sqlalchemy.orm import backref
from . import db
from .urls import Url
from .user import User


class Event(db.Model):
    '''
    <events>
    * id
    * status_code  | Статус ответа
    * url_id  | Идентификатор Web-ресурса
    * response_time  | Время ответа
    * created  | Дата создания записи
    * response_size  | Размер ответа
    active  | Флаг активности события (используется в методе delete)
    '''
    id = db.Column(db.Integer, primary_key=True,
                   autoincrement=True, nullable=False, unique=True)
    status_code = db.Column(db.Integer, nullable=False)

    url_id = db.Column(db.Integer, db.ForeignKey('url.id'), nullable=False)
    url = db.relationship(Url, backref=backref(
        'events'))
    response_time = db.Column(db.Integer, nullable=False)
    created = db.Column(db.DateTime, default=db.func.now())
    response_size = db.Column(db.Integer, nullable=False)

    active = db.Column(db.Boolean, default=False, nullable=False)
