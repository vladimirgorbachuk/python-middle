from exceptions.user import TokenDoesntMatchAnyUserException
from model.user import User


def find_user_by_token(token):
    user = User.query.filter_by(token=token).one_or_none()
    if user:
        return user
    else:
        raise TokenDoesntMatchAnyUserException


# def check_user_permission(user: User, url_id):
#     if user.urls.fetchone
