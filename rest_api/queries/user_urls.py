from exceptions.url import UrlDoesNotExist, UserIsNotOwnerOfGivenURL
from model.user import User
from model.urls import Url
from model import db


def get_all_user_urls(user):
    urls = [url._asdict() for url in
            db.session.query(Url.id, Url.path, Url.owner_id)
            .filter(Url.owner_id == user.id)
            .order_by(Url.created)
            .all()]
    return urls


def add_url_to_user(*, user: User, url_path: str, url_title: str):
    new_url = Url(path=url_path, title=url_title, owner_id=user.id)
    db.session.add(new_url)
    db.session.commit()


def delete_url_if_owner(url_id, user: User):
    url = Url.query.get(url_id)
    if not url:
        raise UrlDoesNotExist(
            'this url id does not correspond to any known url paths')
    else:
        if url.owner_id == user.id:
            db.session.delete(url)
            db.session.commit()
        else:
            raise UserIsNotOwnerOfGivenURL(
                'you are not the owner of this url id')
