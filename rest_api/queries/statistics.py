from model.urls import Url
from model.events import Event
from model import db
from constants.options_enums import SortingOptionsEnum
from typing import Tuple
from sqlalchemy import func, and_, desc
from decimal import Decimal


def collect_statistics(sort_option: SortingOptionsEnum,
                       status_code=None,
                       response_time_tuple=None,
                       ):
    '''
    # /statistic?status_code=301&response_time=100,200&sort=created [GET]
    _Получения статистики мониторинга_

    Параметры запроса:
    - `status_code`=301 - код ответа события (необязательное)
    - `response_time`=100,200 - диапазон времени ответа
    - `sort`=created|response_time|status_code|response_size

    # Пример работы метода

    В таблице `events` имеются следующие записи:
    ```
    url_id                                   |  ...  |  response_time  |  status_code   |  response_size
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  123            |  200           |  76237
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  157            |  200           |  76237
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  196            |  301           |  562
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  83             |  200           |  76237
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  450            |  200           |  67201
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  392            |  200           |  67201
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  1600           |  200           |  67201
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  10050          |  500           |  278
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  823            |  200           |  67921
    ```

    Параметры фильтрации `status_code` и `response_time` отсеивают неподходящие под условия записи,
    после чего происходит группировка по `url_id`, при группировке `response_time` вычисляется как среднее,
    status_code как отношение 2..-х и 3..-х кодов к общему числу событий для данного url, `response_size` как максимальное значение,
    created как последнее значение.
    Результат сортируется в зависимости от параметра `sort`, например `created` (по возрастанию), `-created` (по убыванию).

    Ответ метода с параметрами запроса `status_code=301&response_time=100,200&sort=created` должен выглядеть так:
    ```
    [
        {
            "url_id": "11f895ed-89a9-487d-b110-0feb42dbe761",
            "avg_response_time": 196,
            "success_rate": 1.0,
            "max_response_size": 562
        }
    ]
    ```
    '''
    lower_time = response_time_tuple[0]
    upper_time = response_time_tuple[1]

    query = db.session.query(
        Event.url_id,
        func.avg(Event.response_time),
        func.count().filter(and_(Event.status_code >= 200,
                                 Event.status_code < 400))/func.count(),
        func.max(Event.response_time))

    if status_code:
        query = query.filter(Event.status_code == status_code)

    query = query.filter(
        and_(lower_time < Event.response_time, Event.response_time < upper_time))
    query = query.group_by(Event.url_id)
    query = sort_query_by_sort_option(query, sort_option)

    statistics_list_dicts = [{'url_id': item[0],
                              'avg_response_time':float(item[1]),
                              'succes_rate':item[2],
                              'max_response_size':item[3]} for item in query.all()]
    print(statistics_list_dicts)

    return statistics_list_dicts


def sort_query_by_sort_option(query, sort_option):
    if sort_option == SortingOptionsEnum.CREATED:
        return query.order_by(func.max(Event.created))
    elif sort_option == SortingOptionsEnum.INVERSE_CREATED:
        return query.order_by(desc(func.max(Event.created)))
    elif sort_option == SortingOptionsEnum.RESPONSE_SIZE:
        return query.order_by(func.max(Event.response_size))
    elif sort_option == SortingOptionsEnum.INVERSE_RESPONSE_SIZE:
        return query.order_by(desc(func.max(Event.response_size)))
    elif sort_option == SortingOptionsEnum.RESPONSE_TIME:
        return query.order_by(func.max(Event.response_time))
    elif sort_option == SortingOptionsEnum.INVERSE_RESPONSE_TIME:
        return query.order_by(desc(func.max(Event.response_time)))
    elif sort_option == SortingOptionsEnum.STATUS_CODE:
        return query.order_by(func.max(Event.status_code))
    elif sort_option == SortingOptionsEnum.INVERSE_STATUS_CODE:
        return query.order_by(desc(func.max(Event.status_code)))
