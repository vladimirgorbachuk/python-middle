from model.user import User
from model.urls import Url
from model.events import Event
from model import db


def get_events_slice_count_for_url_by_id(url_id, skip, limit):
    '''
        #### /events/<url_id>?skip=0&limit=10 [GET]
        _Метод получения списка событий по url_

        Параметры запроса:
        - `skip`=0 - количество пропускаемых записей
        - `limit`=10 - количество записей в ответе

        ```
        {
            "items": {
                "url": ...,
                "created": ...,
                "response_time": ...,
                "status_code": ...
            },
            "items_total": 98
        }
        ```
        '''
    events_for_url = [event._asdict() for event in
                      db.session.query(Event.url, Event.created,
                                       Event.response_time, Event.status_code)
                      .filter(Event.url_id == url_id)
                      .order_by(Event.created)
                      .offset(skip)
                      .limit(limit)
                      .all()]

    count_events = db.session.query(Event.url, Event.created,
                                    Event.response_time, Event.status_code).filter(Event.url_id == url_id).count()

    return events_for_url, count_events
