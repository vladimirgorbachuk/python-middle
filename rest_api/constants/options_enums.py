from enum import Enum


class SortingOptionsEnum(Enum):
    '''
    created|response_time|status_code|response_size
    '''
    CREATED = 'created'
    RESPONSE_TIME = 'response_time'
    STATUS_CODE = 'status_code'
    RESPONSE_SIZE = 'response_size'
    INVERSE_CREATED = '-created'
    INVERSE_RESPONSE_TIME = '-response_time'
    INVERSE_STATUS_CODE = '-status_code'
    INVERSE_RESPONSE_SIZE = '-response_size'
