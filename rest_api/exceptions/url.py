class UserIsNotOwnerOfGivenURL(Exception):
    pass


class UrlDoesNotExist(Exception):
    pass
