import flask
import model
from model.user import User
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
#from flask_admin.model import BaseModelView
import os
from flask_migrate import Migrate
import api


app = flask.Flask(__name__)

app.config['SECRET_KEY'] = os.environ['FLASK_SECRET_KEY']
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
db_user = app.config['POSTGRES_USER'] = os.environ['POSTGRES_USER']
db_password = app.config['POSTGRES_PASSWORD'] = os.environ['POSTGRES_PASSWORD']
db_name = app.config['POSTGRES_DB'] = os.environ['POSTGRES_DB']
SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s:%s/%s' % (
    db_user, db_password, 'db', 5432, db_name)
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI

admin = Admin(app, name='url_service_api', template_mode='bootstrap3')

model.db.init_app(app)
model.db.app = app
admin.add_view(ModelView(User, model.db.session))


migrate = Migrate(app, model.db)

app.register_blueprint(api.blueprint, url_prefix="/")


@app.route("/")
def check():
    return "<p>Yep it works!</p>"


if __name__ == "__main__":
    app.run()
