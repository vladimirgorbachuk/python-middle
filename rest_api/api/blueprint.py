import flask
from validation.user import find_user_by_token
from exceptions.user import TokenDoesntMatchAnyUserException
from exceptions.json_data import JsonDoesNotContainRequiredField
from exceptions.url import UrlDoesNotExist, UserIsNotOwnerOfGivenURL

blueprint = flask.Blueprint("api", __name__)


@blueprint.route("/<path:path>")
def unknown_path(path):
    return flask.jsonify(error="Given path `%s` not found" % path), 404


@blueprint.errorhandler(TokenDoesntMatchAnyUserException)
def user_token_not_found(error):
    return flask.jsonify(error='user token is wrong'), 404


@blueprint.errorhandler(JsonDoesNotContainRequiredField)
def json_does_not_contain_required_field(error):
    return flask.jsonify(error=str(error)), 404


@blueprint.errorhandler(UrlDoesNotExist)
def url_does_not_exist(error):
    return flask.jsonify(error=str(error)), 404


@blueprint.errorhandler(UserIsNotOwnerOfGivenURL)
def user_is_not_an_owner(error):
    return flask.jsonify(error=str(error)), 403
