import flask
from flask import request
from .blueprint import blueprint
from validation.user import find_user_by_token
from queries.statistics import collect_statistics
from exceptions.json_data import JsonDoesNotContainRequiredField
from .decorators import add_token_to_response, check_user_token_exists


from constants.options_enums import SortingOptionsEnum


@blueprint.route('/statistic', methods=['GET'])
@add_token_to_response
@check_user_token_exists
def statistics():
    '''
    #### /statistic?status_code=301&response_time=100,200&sort=created [GET]
    _Получения статистики мониторинга_

    Параметры запроса:
    - `status_code`=301 - код ответа события (необязательное)
    - `response_time`=100,200 - диапазон времени ответа
    - `sort`=created|response_time|status_code|response_size

    ##### Пример работы метода

    В таблице `events` имеются следующие записи:
    ```
    url_id                                   |  ...  |  response_time  |  status_code   |  response_size
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  123            |  200           |  76237
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  157            |  200           |  76237
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  196            |  301           |  562
    11f895ed-89a9-487d-b110-0feb42dbe761     |       |  83             |  200           |  76237
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  450            |  200           |  67201
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  392            |  200           |  67201
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  1600           |  200           |  67201
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  10050          |  500           |  278
    0feb42dd-89a9-487d-b110-ed0eb42d742d     |       |  823            |  200           |  67921
    ```

    Параметры фильтрации `status_code` и `response_time` отсеивают неподходящие под условия записи,
    после чего происходит группировка по `url_id`, при группировке `response_time` вычисляется как среднее,
    status_code как отношение 2..-х и 3..-х кодов к общему числу событий для данного url, `response_size` как максимальное значение,
    created как последнее значение.
    Результат сортируется в зависимости от параметра `sort`, например `created` (по возрастанию), `-created` (по убыванию).

    Ответ метода с параметрами запроса `status_code=301&response_time=100,200&sort=created` должен выглядеть так:
    ```
    [
        {
            "url_id": "11f895ed-89a9-487d-b110-0feb42dbe761",
            "avg_response_time": 196,
            "success_rate": 1.0,
            "max_response_size": 562
        }
    ]
    ```
    '''

    status_code = request.args.get('status_code')
    response_time_tuple_as_str = request.args.get('response_time')
    response_time_tuple = tuple([int(time_str)
                                 for time_str in response_time_tuple_as_str.split(',')])
    sorting_option_str = request.args['sort']
    sorting_option = SortingOptionsEnum(sorting_option_str)
    statistics_dicts_list = collect_statistics(sorting_option,
                                               status_code=status_code, response_time_tuple=response_time_tuple)

    return flask.jsonify(statistics_dicts_list)
