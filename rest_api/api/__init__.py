from .blueprint import blueprint
from . import urls, events, statistics

__all__ = [
    "blueprint"
]
