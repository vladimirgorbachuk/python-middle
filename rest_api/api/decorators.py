from flask import make_response, request
from validation.user import find_user_by_token
import functools


def add_token_to_response(view_func):
    @functools.wraps(view_func)
    def wrapped_func(*args, **kwargs):
        response = make_response(view_func(*args, **kwargs))
        token = request.headers.get('Token')
        response.headers['Token'] = token
        return response
    return wrapped_func


def check_user_token_exists(view_func):
    @functools.wraps(view_func)
    def wrapped_func(*args, **kwargs):
        token = request.headers.get('Token')
        user = find_user_by_token(token)
        return view_func(*args, **kwargs)
    return wrapped_func
