import flask
from flask import request
from .blueprint import blueprint
from validation.user import find_user_by_token
from queries.url_events import get_events_slice_count_for_url_by_id
from exceptions.json_data import JsonDoesNotContainRequiredField
from .decorators import add_token_to_response, check_user_token_exists


@blueprint.route('/events/<int:url_id>', methods=['GET'])
@add_token_to_response
@check_user_token_exists
def get_url_events(*, url_id):
    '''
        #### /events/<url_id>?skip=0&limit=10 [GET]
        _Метод получения списка событий по url_

        Параметры запроса:
        - `skip`=0 - количество пропускаемых записей
        - `limit`=10 - количество записей в ответе

        ```
        {
            "items": {
                "url": ...,
                "created": ...,
                "response_time": ...,
                "status_code": ...
            },
            "items_total": 98
        }
    '''
    skip = request.args['skip']
    limit = request.args['limit']
    events, count = get_events_slice_count_for_url_by_id(url_id, skip, limit)
    return flask.jsonify({'items': events, 'items_total': count})
