import flask
from flask import request
from .blueprint import blueprint
from validation.user import find_user_by_token
from queries.user_urls import (get_all_user_urls,
                               add_url_to_user,
                               delete_url_if_owner)
from exceptions.json_data import JsonDoesNotContainRequiredField
from .decorators import add_token_to_response, check_user_token_exists


@blueprint.route('/urls', methods=['GET'])
@add_token_to_response
@check_user_token_exists
def user_urls_list():
    '''
    _Список Web-ресурсов пользователя_
    [
        {
            "id": ...,
            "path": "https://.....",
        },
        {
            "id": ...,
            "path": "https://.....",
        }
    ]
    '''
    user_token = request.headers.get('token')
    user = find_user_by_token(user_token)
    url_list = get_all_user_urls(user)
    return flask.jsonify(url_list)


@blueprint.route('/urls', methods=['POST'])
@add_token_to_response
@check_user_token_exists
def add_url_for_user():
    '''
    _Добавление url в мониторинг_ по ключу 'url_path'
    '''
    user_token = request.headers.get('token')
    user = find_user_by_token(user_token)
    json_data = request.json
    url_path = json_data.get('url_path')
    url_title = json_data.get('url_title')
    if url_path and url_title:
        add_url_to_user(user=user, url_path=url_path, url_title=url_title)
    else:
        raise JsonDoesNotContainRequiredField(
            'please provide two fields: "url_path" and "url_title"')
    return flask.jsonify({'status': f'{url_path}, titled as {url_title} has been added successfully',
                          'status_code': 201})


@blueprint.route('/urls/<int:url_id>', methods=['DELETE'])
@add_token_to_response
@check_user_token_exists
def remove_url_for_user(*, url_id):
    '''
    _Удаления url из мониторинга (делать неактивным)_
    '''
    user_token = request.headers.get('token')
    user = find_user_by_token(user_token)
    delete_url_if_owner(url_id, user)
    return flask.jsonify({'status': f'{url_id} has been deleted successfully',
                          'status_code': 204})
